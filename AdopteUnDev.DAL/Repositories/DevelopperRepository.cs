﻿using AdopteUnDev.DAL.Entities;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ToolBox.ADO.Utils;

namespace AdopteUnDev.DAL.Repositories
{
    public class DevelopperRepository : BaseRepository<Developper>
    {
        public DevelopperRepository(string connectionString, string providerName) : base(connectionString, providerName)
        {
        }

        public void AddSkillDevelopper(int idDev, int idSkill)
        {
            using (IDbConnection conn = GetConnection())
            {
                conn.Open();
                string query = "INSERT INTO [Dev_Skill] (IdSkill, IdDevelopper) values(@idS, @idD)";
                Dictionary<string, object> dico = new Dictionary<string, object>
                    {
                        { "@idS", idSkill },
                        { "@idD", idDev },
                };
                IDbCommand cmd = CreateCommand(conn, query, dico);
                cmd.ExecuteNonQuery();
            }
        }

        public void removeSkillFromDev(int idSkill, int idDev)
        {
            using (IDbConnection conn = GetConnection())
            {
                conn.Open();
                string query = "DELETE FROM [Dev_Skill] WHERE idSkill = @idS AND idDevelopper = @idD";
                Dictionary<string, object> dico = new Dictionary<string, object>
                    {
                        { "@idS", idSkill },
                        { "@idD", idDev },
                };
                IDbCommand cmd = CreateCommand(conn, query, dico);
                cmd.ExecuteNonQuery();
            }
        }

        public virtual IEnumerable<int> getDevSkill(int idD)
        {
            using (IDbConnection conn = GetConnection())
            {
                conn.Open();
                string query = "SELECT IdSkill FROM [Dev_Skill] WHERE IdDevelopper = @p1";
                Dictionary<string, object> dico = new Dictionary<string, object>
                    {
                        { "@p1", idD },
                    };
                IDbCommand cmd = CreateCommand(conn, query, dico);
                IDataReader r = cmd.ExecuteReader();
                while (r.Read())
                {
                    yield return (int)r["IdSkill"];
                }
            }
        }
    }
}
