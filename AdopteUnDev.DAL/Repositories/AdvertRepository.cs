﻿using AdopteUnDev.DAL.Entities;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ToolBox.ADO.Utils;


namespace AdopteUnDev.DAL.Repositories
{
    public class AdvertRepository : BaseRepository<Advert>
    {
        public AdvertRepository(string connectionString, string providerName) : base(connectionString, providerName)
        {
        }

        public virtual IEnumerable<Advert> GetByIdEnterprise(int id)
        {
            using (IDbConnection conn = GetConnection())
            {
                conn.Open();
                string query = "SELECT * FROM [Advert] WHERE IdEnterprise = @p1";
                Dictionary<string, object> dico = new Dictionary<string, object>
                    {
                        { "@p1", id }
                    };
                IDbCommand cmd = CreateCommand(conn, query, dico);
                IDataReader r = cmd.ExecuteReader();
                while (r.Read())
                {
                    yield return new Advert
                    {
                        Id = (int)r["Id"],
                        Name = (string)r["Name"],
                        Date = (DateTime)r["Date"],
                        Description = (string)r["Description"],
                        IdEnterprise = (int)r["IdEnterprise"]
                    };
                }
            }
        }
        public void AddSkillToAdvert(int idAdvert, int idSkill)
        {
            using (IDbConnection conn = GetConnection())
            {
                conn.Open();
                string query = "INSERT INTO [Advert_Skill] (IdSkill, IdAdvert) values(@idS, @idA)";
                Dictionary<string, object> dico = new Dictionary<string, object>
                    {
                        { "@idS", idSkill },
                        { "@idA", idAdvert },
                };
                IDbCommand cmd = CreateCommand(conn, query, dico);
                cmd.ExecuteNonQuery();
            }
        }

        public void removeSkillFromAdvert(int idSkill, int idAdvert)
        {
            using (IDbConnection conn = GetConnection())
            {
                conn.Open();
                string query = "DELETE FROM [Advert_Skill] WHERE idSkill = @idS AND idAdvert = @idA";
                Dictionary<string, object> dico = new Dictionary<string, object>
                    {
                        { "@idS", idSkill },
                        { "@idA", idAdvert },
                };
                IDbCommand cmd = CreateCommand(conn, query, dico);
                cmd.ExecuteNonQuery();
            }
        }

        public virtual IEnumerable<int> getAdvertSkill(int idA)
        {
            using (IDbConnection conn = GetConnection())
            {
                conn.Open();
                string query = "SELECT IdSkill FROM [Advert_Skill] WHERE IdAdvert = @p1";
                Dictionary<string, object> dico = new Dictionary<string, object>
                    {
                        { "@p1", idA },
                    };
                IDbCommand cmd = CreateCommand(conn, query, dico);
                IDataReader r = cmd.ExecuteReader();
                while (r.Read())
                {
                    yield return (int)r["IdSkill"];
                }
            }
        }



        public virtual IEnumerable<Advert> GetBySkillShare(int id)
        {

            using (IDbConnection conn = GetConnection())
            {
                conn.Open();
                string query = 
                    "SELECT * from [Advert] " +
                    "WHERE Id IN "+
                    "(SELECT DISTINCT A.IdAdvert " +
                    "FROM [Developper] as D " +
                    "inner join Dev_Skill as S " +
                    "on D.Id = S.IdDevelopper " +
                    "inner join Advert_Skill as A " +
                    "on S.IdSkill = A.IdSkill " +
                    "WHERE D.Id = @p1) ";
                Dictionary<string, object> dico = new Dictionary<string, object>
                    {
                        { "@p1", id }
                    };
                IDbCommand cmd = CreateCommand(conn, query, dico);
                IDataReader r = cmd.ExecuteReader();
                while (r.Read())
                {
                    yield return new Advert
                    {
                        Id = (int)r["Id"],
                        Name = (string)r["Name"],
                        Date = (DateTime)r["Date"],
                        Description = (string)r["Description"],
                        IdEnterprise = (int)r["IdEnterprise"]
                    };
                }
            }
        }
    }
}
