﻿using AdopteUnDev.DAL.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ToolBox.ADO.Utils;

namespace AdopteUnDev.DAL.Repositories
{
    public class EnterpriseRepository : BaseRepository<Enterprise>
    {
        public EnterpriseRepository(string connectionString, string providerName) : base(connectionString, providerName)
        {
        }
    }
}
