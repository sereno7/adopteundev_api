﻿using AdopteUnDev.DAL.Entities;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ToolBox.ADO.Utils;

namespace AdopteUnDev.DAL.Repositories
{
    public class UserRepository : BaseRepository<User>
    {
        public UserRepository(string connectionString, string providerName) : base(connectionString, providerName)
        {
        }

        public virtual User GetLoginUser(string username, string password)
        {
            using (IDbConnection conn = GetConnection())
            {
                conn.Open();
                string query = "SELECT * FROM [User] WHERE UserName = @p1 AND Password = @p2";
                Dictionary<string, object> dico = new Dictionary<string, object>
                    {
                        { "@p1", username },
                        { "@p2", password }
                    };
                IDbCommand cmd = CreateCommand(conn, query, dico);
                IDataReader r = cmd.ExecuteReader();
                if (r.Read())
                {
                    return new User
                    {
                        Id = (int)r["Id"],
                        FirstName =(string)r["FirstName"],
                        LastName =(string)r["LastName"],
                        UserName =(string)r["UserName"],
                        Password =(string)r["Password"],
                        //Token =(string)r["Token"],
                    };
                }
                return null;

            }
        }
        
    }
}
