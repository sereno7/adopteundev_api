﻿using AdopteUnDev.DAL.Entities;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ToolBox.ADO.Utils;

namespace AdopteUnDev.DAL.Repositories
{
    public class MessageRepository : BaseRepository<Message>
    {
        public MessageRepository(string connectionString, string providerName) : base(connectionString, providerName)
        {
            
        }
        public virtual IEnumerable<Message> GetMessageExchange(int idA, int idD)
        {
            using (IDbConnection conn = GetConnection())
            {
                conn.Open();
                string query = "SELECT * FROM [Message] WHERE IdDevelopper = @p1 AND IdAdvert = @p2 ORDER BY DATE";
                Dictionary<string, object> dico = new Dictionary<string, object>
                    {
                        { "@p1", idD },
                        { "@p2", idA }
                    };
                IDbCommand cmd = CreateCommand(conn, query, dico);
                IDataReader r = cmd.ExecuteReader();
                while (r.Read())
                {
                    yield return new Message
                    {
                        Id = (int)r["Id"],
                        Name = (string)r["Name"],
                        Subject = (string)r["Subject"],
                        Date = (DateTime)r["Date"],
                        IdAdvert = (int)r["IdAdvert"],
                        IdDevelopper = (int)r["IdDevelopper"]
                    };
                }
            }
        }
    }
}
