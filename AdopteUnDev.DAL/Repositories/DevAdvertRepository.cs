﻿using AdopteUnDev.DAL.Entities;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ToolBox.ADO.Utils;

namespace AdopteUnDev.DAL.Repositories
{
    public class DevAdvertRepository : BaseRepository<DevAdvert>
    {
        public DevAdvertRepository(string connectionString, string providerName) : base(connectionString, providerName)
        {
        }




        public virtual IEnumerable<DevAdvert> getLikedStatus(int idD)
        {
            using (IDbConnection conn = GetConnection())
            {
                conn.Open();
                string query = "SELECT * FROM [Advert_Dev] WHERE IdDevelopper = @p1";
                Dictionary<string, object> dico = new Dictionary<string, object>
                    {
                        { "@p1", idD },
                    };
                IDbCommand cmd = CreateCommand(conn, query, dico);
                IDataReader r = cmd.ExecuteReader();
                while (r.Read())
                {
                    yield return new DevAdvert
                    {
                        IdAdvert = (int)r["IdAdvert"],
                        IdDevelopper = (int)r["IdDevelopper"],
                        AdvertAcceptDev = (bool)r["AdvertAcceptDev"],
                        DevAcceptAdvert = (bool)r["DevAcceptAdvert"]
                    };
                }
            }
        }

        public virtual IEnumerable<DevAdvert> getLikedByAdvertStatus(int idA)
        {
            using (IDbConnection conn = GetConnection())
            {
                conn.Open();
                string query = "SELECT * FROM [Advert_Dev] WHERE IdAdvert = @p1";
                Dictionary<string, object> dico = new Dictionary<string, object>
                    {
                        { "@p1", idA },
                    };
                IDbCommand cmd = CreateCommand(conn, query, dico);
                IDataReader r = cmd.ExecuteReader();
                while (r.Read())
                {
                    yield return new DevAdvert
                    {
                        IdAdvert = (int)r["IdAdvert"],
                        IdDevelopper = (int)r["IdDevelopper"],
                        AdvertAcceptDev = (bool)r["AdvertAcceptDev"],
                        DevAcceptAdvert = (bool)r["DevAcceptAdvert"]
                    };
                }
            }
        }




        public void AddLikeToAdvert(int idAdvert, int idDevelopper)
        {
            using (IDbConnection conn = GetConnection())
            {
                conn.Open();
                string query = "INSERT INTO [Advert_Dev] (IdDevelopper, IdAdvert, DevAcceptAdvert) values(@idD, @idA, @bool)";
                Dictionary<string, object> dico = new Dictionary<string, object>
                    {
                        { "@idD", idDevelopper },
                        { "@idA", idAdvert },
                        { "@bool", true },
                };
                IDbCommand cmd = CreateCommand(conn, query, dico);
                cmd.ExecuteNonQuery();
            }
        }

        public void RemoveLikeFromAdvert(int idDevelopper, int idAdvert)
        {
            using (IDbConnection conn = GetConnection())
            {
                conn.Open();
                string query = "DELETE FROM [Advert_Dev] WHERE idDevelopper = @idD AND idAdvert = @idA";
                Dictionary<string, object> dico = new Dictionary<string, object>
                    {
                        { "@idD", idDevelopper },
                        { "@idA", idAdvert },
                };
                IDbCommand cmd = CreateCommand(conn, query, dico);
                cmd.ExecuteNonQuery();
            }
        }

        public void AddLikeToDev(int idAdvert, int idDevelopper)
        {
            using (IDbConnection conn = GetConnection())
            {
                conn.Open();
                string query = "UPDATE [Advert_Dev] SET AdvertAcceptDev = @bool WHERE IdDevelopper = @idD AND iDAdvert = @idA";
                Dictionary<string, object> dico = new Dictionary<string, object>
                    {
                        { "@idD", idDevelopper },
                        { "@idA", idAdvert },
                        { "@bool", true },
                };
                IDbCommand cmd = CreateCommand(conn, query, dico);
                cmd.ExecuteNonQuery();
            }
        }

        public void RemoveLikeFromDev(int idDevelopper, int idAdvert)
        {
            using (IDbConnection conn = GetConnection())
            {
                conn.Open();
                string query = "UPDATE [Advert_Dev] SET AdvertAcceptDev = @bool WHERE IdDevelopper = @idD AND iDAdvert = @idA";
                Dictionary<string, object> dico = new Dictionary<string, object>
                    {
                        { "@idD", idDevelopper },
                        { "@idA", idAdvert },
                        { "@bool", false },
                };
                IDbCommand cmd = CreateCommand(conn, query, dico);
                cmd.ExecuteNonQuery();
            }
        }

        public IEnumerable<Developper> GetDevByLikedAdvert(int idA)
        {
            using (IDbConnection conn = GetConnection())
            {
                conn.Open();
                string query = "SELECT * FROM [Developper] WHERE ID IN(SELECT IdDevelopper FROM [Advert_Dev] WHERE IDAdvert = @p1)";
                Dictionary<string, object> dico = new Dictionary<string, object>
                    {
                        { "@p1", idA },
                    };
                IDbCommand cmd = CreateCommand(conn, query, dico);
                IDataReader r = cmd.ExecuteReader();
                while (r.Read())
                {
                    yield return new Developper
                    {
                        Id = (int)r["Id"],
                        First_Name = (string)r["First_Name"],
                        Last_Name = (string)r["Last_Name"],
                        Email = (string)r["Email"],
                        Adresse = (string)r["Adresse"],
                        Birth_date = (DateTime)r["Birth_date"],
                        Tel = (string)r["Tel"]
                    };
                };
            }
        }
    }
}


