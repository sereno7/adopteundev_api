﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AdopteUnDev.DAL.Entities
{
    public class Enterprise
    {
		public int Id { get; set; }
		public string Name { get; set; }
		public string Email { get; set; }
		public string Adresse { get; set; }
		public string Tel { get; set; }
		public string Fax { get; set; }
		public string Tva { get; set; }
	}
}

