﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AdopteUnDev.DAL.Entities
{
    public class Message
    {
		public int Id { get; set; }
		public string Name { get; set; }
		public string Subject { get; set; }
		public DateTime Date { get; set; }
		public int IdDevelopper { get; set; }
		public int IdAdvert { get; set; }
	}
}
