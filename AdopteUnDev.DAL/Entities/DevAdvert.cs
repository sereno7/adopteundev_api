﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AdopteUnDev.DAL.Entities
{
    public class DevAdvert
    {
        public int Id { get; set; }
        public int IdAdvert { get; set; }
        public int IdDevelopper { get; set; }
        public bool DevAcceptAdvert { get; set; }
        public bool AdvertAcceptDev { get; set; }
    }
}
