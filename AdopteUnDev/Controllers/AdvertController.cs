﻿using AdopteUnDev.DAL.Entities;
using AdopteUnDev.DAL.Repositories;
using AdopteUnDev.DI;
using AdopteUnDev.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;

namespace AdopteUnDev.Controllers
{
    public class AdvertController : ApiController
    {
        [HttpGet]
        public IEnumerable<AdvertModel> Get()
        {
            AdvertRepository repo = ServicesLocator.GetService<AdvertRepository>();
            IEnumerable<Advert> Adverts = repo.Get();
            IEnumerable<AdvertModel> models = Adverts.Select(x =>
                new AdvertModel
                {
                    Id = x.Id,
                    Name = x.Name,
                    Date = x.Date,
                    Description = x.Description,
                    IdEnterprise = x.IdEnterprise
                });
            return models;
        }


        [HttpGet]
        public AdvertModel Get(int id)
        {
            AdvertRepository repo = ServicesLocator.GetService<AdvertRepository>();
            Advert x = repo.GetById(id);
            AdvertModel models = new AdvertModel
            {
                Id = x.Id,
                Name = x.Name,
                Date = x.Date,
                Description = x.Description,
                IdEnterprise = x.IdEnterprise
            };
            return models;
        }

        [HttpGet]
        [Route("api/Advert/GetByIdEnterprise/{id}")]
        public IEnumerable<AdvertModel> GetByIdEnterprise(int id)
        {
            AdvertRepository repo = ServicesLocator.GetService<AdvertRepository>();
            IEnumerable<Advert> adverts = repo.GetByIdEnterprise(id);
            IEnumerable<AdvertModel> models = adverts.Select(m =>
            new AdvertModel
            {
                Id = m.Id,
                Name = m.Name,
                Date = m.Date,
                Description = m.Description,
                IdEnterprise = m.IdEnterprise
            });
            return models;
        }


        [HttpPost]
        public int Post(AdvertModel x)
        {
            AdvertRepository repo = ServicesLocator.GetService<AdvertRepository>();
            Advert models = new DAL.Entities.Advert
            {
                Id = x.Id,
                Name = x.Name,
                Date = x.Date,
                Description = x.Description,
                IdEnterprise = x.IdEnterprise
            };
            return repo.Insert(models);
        }


        [HttpPut]
        public void Put(AdvertModel x)
        {
            AdvertRepository repo = ServicesLocator.GetService<AdvertRepository>();
            Advert models = new Advert
            {
                Id = x.Id,
                Name = x.Name,
                Date = x.Date,
                Description = x.Description,
                IdEnterprise = x.IdEnterprise
            };
            repo.Update(models);
        }


        [HttpDelete]
        public void Delete(int id)
        {
            AdvertRepository repo = ServicesLocator.GetService<AdvertRepository>();
            repo.Delete(id);
        }


        [HttpPost]
        [Route("api/advert/AddSkillToAdvert")]
        public void addSkillToAdvert(AdvertSkillModel model)
        {
            AdvertRepository repo = ServicesLocator.GetService<AdvertRepository>();
            repo.AddSkillToAdvert(model.IdAdvert, model.IdSkill);
        }

        [HttpPost]
        [Route("api/advert/removeSkillFromAdvert")]
        public void removeSkillFromAdvert(AdvertSkillModel model)
        {
            AdvertRepository repo = ServicesLocator.GetService<AdvertRepository>();
            repo.removeSkillFromAdvert(model.IdSkill, model.IdAdvert);
        }

        [HttpGet]
        [Route("api/advert/getAdvertSkill/{idA}")]
        public IEnumerable<int> getAdvertSkill(int idA)
        {
            AdvertRepository repo = ServicesLocator.GetService<AdvertRepository>();
            IEnumerable<int> AdvertSkillModel = repo.getAdvertSkill(idA);
            return AdvertSkillModel;
        }

        [HttpGet]
        [Route("api/advert/getBySkillShare/{idD}")]
        public IEnumerable<AdvertModel> GetBySkillShare(int idD)
        {
            AdvertRepository repo = ServicesLocator.GetService<AdvertRepository>();
            IEnumerable<Advert> Adverts = repo.GetBySkillShare(idD);
            IEnumerable<AdvertModel> models = Adverts.Select(x =>
                new AdvertModel
                {
                    Id = x.Id,
                    Name = x.Name,
                    Date = x.Date,
                    Description = x.Description,
                    IdEnterprise = x.IdEnterprise
                });
            return models;
        }


        [HttpPost]
        [Route("api/advert/AddLikeToAdvert")]
        public void AddLikeToAdvert(devAdvertModel model)
        {
            DevAdvertRepository repo = ServicesLocator.GetService<DevAdvertRepository>();
            repo.AddLikeToAdvert(model.IdAdvert, model.IdDevelopper);
        }

        [HttpPost]
        [Route("api/advert/RemoveLikeFromAdvert")]
        public void RemoveLikeFromAdvert(devAdvertModel model)
        {
            DevAdvertRepository repo = ServicesLocator.GetService<DevAdvertRepository>();
            repo.RemoveLikeFromAdvert(model.IdDevelopper, model.IdAdvert);
        }





        [HttpPut]
        [Route("api/advert/AddLikeToDev")]
        public void AddLikeToDev(devAdvertModel model)
        {
            DevAdvertRepository repo = ServicesLocator.GetService<DevAdvertRepository>();
            repo.AddLikeToDev(model.IdAdvert, model.IdDevelopper);
        }

        [HttpPut]
        [Route("api/advert/RemoveLikeFromDev")]
        public void RemoveLikeFromDev(devAdvertModel model)
        {
            DevAdvertRepository repo = ServicesLocator.GetService<DevAdvertRepository>();
            repo.RemoveLikeFromDev(model.IdDevelopper, model.IdAdvert);
        }










        [HttpGet]
        [Route("api/advert/getLikedByAdvertStatus/{IdA}")]
        public IEnumerable<devAdvertModel> getLikedByAdvertStatus(int IdA)
        {
            DevAdvertRepository repo = ServicesLocator.GetService<DevAdvertRepository>();
            IEnumerable<DevAdvert> liste = repo.getLikedByAdvertStatus(IdA);
            IEnumerable<devAdvertModel> models = liste.Select(x =>
                new devAdvertModel
                {
                    IdAdvert = x.IdAdvert,
                    IdDevelopper = x.IdDevelopper,
                    AdvertAcceptDev = x.AdvertAcceptDev,
                    DevAcceptAdvert = x.DevAcceptAdvert
                });
            return models;
        }



        [HttpGet]
        [Route("api/advert/getLikedStatus/{IdD}")]
        public IEnumerable<devAdvertModel> getLikedStatus(int IdD)
        {
            DevAdvertRepository repo = ServicesLocator.GetService<DevAdvertRepository>();
            IEnumerable<DevAdvert> liste = repo.getLikedStatus(IdD);
            IEnumerable<devAdvertModel> models = liste.Select(x =>
                new devAdvertModel
                {
                    IdAdvert = x.IdAdvert,
                    IdDevelopper = x.IdDevelopper,
                    AdvertAcceptDev = x.AdvertAcceptDev,
                    DevAcceptAdvert = x.DevAcceptAdvert
                });
            return models;
        }






        [HttpGet]
        [Route("api/advert/GetDevByLikedAdvert/{idA}")]
        public IEnumerable<Developper> GetDevByLikedAdvert(int idA)
        {
            DevAdvertRepository repo = ServicesLocator.GetService<DevAdvertRepository>();
            IEnumerable<Developper> DevList = repo.GetDevByLikedAdvert(idA);
            return DevList;
        }
    }
}