﻿using AdopteUnDev.DAL.Entities;
using AdopteUnDev.DAL.Repositories;
using AdopteUnDev.DI;
using AdopteUnDev.Models;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Http;


namespace AdopteUnDev.Controllers
{
    public class UserController : ApiController
    {
        // GET: User
        [HttpGet]
        public IEnumerable<UserModel> Get()
        {
            UserRepository repo = ServicesLocator.GetService<UserRepository>();
            IEnumerable<User> Users = repo.Get();
            IEnumerable<UserModel> models = Users.Select(x =>
                new UserModel
                {
                     Id = x.Id,
                     UserName = x.UserName,
                     Password = x.Password,
                     FirstName = x.FirstName,
                     LastName = x.LastName,
                     Token = x.Token,
                });
            return models;
        }


        [HttpPost]
        public void Post(UserModel model)
        {
            UserRepository repo = ServicesLocator.GetService<UserRepository>();
            User models = new User
            {
                Id = model.Id,
                UserName = model.UserName,
                Password = model.Password,
                FirstName = model.FirstName,
                LastName = model.LastName,
                Token = model.Token,
            };
            repo.Insert(models);
        }


        [HttpDelete]
        public void Delete(int id)
        {
            UserRepository repo = ServicesLocator.GetService<UserRepository>();
            repo.Delete(id);
        }

        [HttpPost]
        [Route("api/user/authenticate")]

        public UserModel authenticate(LoginModel login)
        {
            UserModel user = GetLoginUser(login.username, login.password);
            if (user != null)
                return user;
            return null;
        }


        private UserModel GetLoginUser(string username, string password)
        {
            UserRepository repo = ServicesLocator.GetService<UserRepository>();
            User Users = repo.GetLoginUser(username, password);
            if(Users != null)
            {
                UserModel models = new UserModel
                {
                    Id = Users.Id,
                    UserName = Users.UserName,
                    Password = Users.Password,
                    FirstName = Users.FirstName,
                    LastName = Users.LastName,
                    Token = "fake-jwt-token",
                };
                return models;
            }
            return null;
            
            
        }
        //private string GenerateJSONWebToken(UserModel userInfo)
        //{
        //    var securityKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_config["Jwt:Key"]));
        //    var credentials = new SigningCredentials(securityKey, SecurityAlgorithms.HmacSha256);

        //    var token = new JwtSecurityToken(_config["Jwt:Issuer"],
        //      _config["Jwt:Issuer"],
        //      null,
        //      expires: DateTime.Now.AddMinutes(120),
        //      signingCredentials: credentials);

        //    return new JwtSecurityTokenHandler().WriteToken(token);
        //}
    }
}
