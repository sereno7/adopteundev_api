﻿using AdopteUnDev.DAL.Entities;
using AdopteUnDev.DAL.Repositories;
using AdopteUnDev.DI;
using AdopteUnDev.Filters;
using AdopteUnDev.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;

namespace AdopteUnDev.Controllers
{
    public class SkillController : ApiController
    {
        [HttpGet]
        public IEnumerable<SkillModel> Get()
        {
            SkillRepository repo = ServicesLocator.GetService<SkillRepository>();
            IEnumerable<Skill> skills = repo.Get();
            IEnumerable<SkillModel> models = skills.Select(x =>
                new SkillModel
                {
                    Id = x.Id,
                    Name = x.Name,
                });
            return models;       
        }


        [HttpGet]
        public SkillModel Get(int id)
        {
            SkillRepository repo = ServicesLocator.GetService<SkillRepository>();
            Skill skills = repo.GetById(id);
            SkillModel models = new SkillModel
                {
                    Id = skills.Id,
                    Name = skills.Name,
                };
            return models;
        }



        [HttpPost]
        [API_Authorize]
        public void Post(SkillModel model)
        {
            SkillRepository repo = ServicesLocator.GetService<SkillRepository>();           
            Skill models = new Skill
            {
                Id = model.Id,
                Name = model.Name,
            };
            repo.Insert(models);
        }



        [HttpPut]
        public void Put(SkillModel m)
        {
            SkillRepository repo = ServicesLocator.GetService<SkillRepository>();
            Skill models = new Skill
            {
                Id = m.Id,
                Name = m.Name,
            };
            repo.Update(models);
        }




        [HttpDelete]
        public void Delete(int id)
        {
            SkillRepository repo = ServicesLocator.GetService<SkillRepository>();
            repo.Delete(id);
        }




    }
}