﻿using AdopteUnDev.DAL.Entities;
using AdopteUnDev.DAL.Repositories;
using AdopteUnDev.DI;
using AdopteUnDev.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;


namespace AdopteUnDev.Controllers
{
    public class MessageController : ApiController
    {
        [HttpGet]
        [Route("api/message/GetMessageExchange/{idA}/{idD}")]
        public IEnumerable<MessageModel> GetMessageExchange(int idA, int idD)
        {
            MessageRepository repo = ServicesLocator.GetService<MessageRepository>();
            IEnumerable<Message> Messages = repo.GetMessageExchange(idA, idD);
            IEnumerable<MessageModel> models = Messages.Select(x =>
                new MessageModel
                {
                    Id = x.Id,
                    Name = x.Name,
                    Date = x.Date,
                    Subject = x.Subject,
                    IdDevelopper = x.IdDevelopper,
                    IdAdvert = x.IdAdvert
                });
            return models;
        }

        [HttpPost]
        public int Post(MessageModel x)
        {
            MessageRepository repo = ServicesLocator.GetService<MessageRepository>();
            Message models = new Message
            {
                Id = x.Id,
                Date = x.Date,
                Name = x.Name,
                Subject = x.Subject,
                IdDevelopper = x.IdDevelopper,
                IdAdvert = x.IdAdvert
            };
            return repo.Insert(models);
        }

        [HttpDelete]
        public void Delete(int id)
        {
            MessageRepository repo = ServicesLocator.GetService<MessageRepository>();
            repo.Delete(id);
        }

    }
}