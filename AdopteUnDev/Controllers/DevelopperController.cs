﻿using AdopteUnDev.DAL.Entities;
using AdopteUnDev.DAL.Repositories;
using AdopteUnDev.DI;
using AdopteUnDev.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;



namespace AdopteUnDev.Controllers
{
    public class DevelopperController : ApiController
    {
        [HttpGet]
        public IEnumerable<DevelopperModel> Get()
        {
            DevelopperRepository repo = ServicesLocator.GetService<DevelopperRepository>();
            IEnumerable<Developper> developper = repo.Get();
            IEnumerable<DevelopperModel> models = developper.Select(x =>
                new DevelopperModel
                {
                    Id = x.Id,
                    Last_Name = x.Last_Name,
                    First_Name = x.First_Name,
                    Email = x.Email,
                    Adresse = x.Adresse,
                    Tel = x.Tel,
                    Birth_date = x.Birth_date
                });
            return models;
        }



        [HttpGet]
        public DevelopperDetailsModel Get(int id)
        {
            DevelopperRepository repo = ServicesLocator.GetService<DevelopperRepository>();
            AdvertRepository repoAdvert = ServicesLocator.GetService<AdvertRepository>();
            Developper x = repo.GetById(id);
            DevelopperDetailsModel dev = new DevelopperDetailsModel
            {
                Id = x.Id,
                Last_Name = x.Last_Name,
                First_Name = x.First_Name,
                Email = x.Email,
                Adresse = x.Adresse,
                Tel = x.Tel,
                Birth_date = x.Birth_date,
                Adverts = repoAdvert.GetBySkillShare(id).Select(y =>
                new AdvertModel
                {
                    Id = y.Id,
                    Name = y.Name,
                    Date = y.Date,
                    Description = y.Description,
                    IdEnterprise = y.IdEnterprise
                })
            };
            return dev;
        }



        [HttpPost]
        public int Post(DevelopperModel x)
        {
            DevelopperRepository repo = ServicesLocator.GetService<DevelopperRepository>();
            Developper model = new Developper
            {
                Id = x.Id,
                Last_Name = x.Last_Name,
                First_Name = x.First_Name,
                Email = x.Email,
                Adresse = x.Adresse,
                Tel = x.Tel,
                Birth_date = x.Birth_date
            };
            return repo.Insert(model);
        }



        [HttpPut]
        public void Put(DevelopperModel x)
        {
            DevelopperRepository repo = ServicesLocator.GetService<DevelopperRepository>();
            Developper dev = new Developper
            {
                Id = x.Id,
                Last_Name = x.Last_Name,
                First_Name = x.First_Name,
                Email = x.Email,
                Adresse = x.Adresse,
                Tel = x.Tel,
                Birth_date = x.Birth_date
            };
            repo.Update(dev);
        }




        [HttpDelete]
        public void Delete(int id)
        {
            DevelopperRepository repo = ServicesLocator.GetService<DevelopperRepository>();
            repo.Delete(id);
        }


        [HttpPost]
        [Route("api/Developper/AddSkillToDevelopper")]
        public void addSkillToAdvert(DevelopperSkillModel model)
        {
            DevelopperRepository repo = ServicesLocator.GetService<DevelopperRepository>();
            repo.AddSkillDevelopper(model.IdDevelopper , model.IdSKill);
        }

        [HttpPost]
        [Route("api/Developper/RemoveSkillFromDevelopper")]
        public void removeSkillFromAdvert(DevelopperSkillModel model)
        {
            DevelopperRepository repo = ServicesLocator.GetService<DevelopperRepository>();
            repo.removeSkillFromDev(model.IdSKill, model.IdDevelopper);
        }

        [HttpGet]
        [Route("api/Developper/getDevelopperSkill/{idD}")]
        public IEnumerable<int> getDevSkill(int idD)
        {
            DevelopperRepository repo = ServicesLocator.GetService<DevelopperRepository>();
            IEnumerable<int> DevelopperSkill = repo.getDevSkill(idD);
            return DevelopperSkill;
        }
    }
}






