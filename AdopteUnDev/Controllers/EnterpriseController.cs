﻿using AdopteUnDev.DAL.Entities;
using AdopteUnDev.DAL.Repositories;
using AdopteUnDev.DI;
using AdopteUnDev.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;

namespace AdopteUnDev.Controllers
{
    public class EnterpriseController : ApiController
    {
        [HttpGet]
        public IEnumerable<EnterpriseModel> Get()
        {
            EnterpriseRepository repo = ServicesLocator.GetService<EnterpriseRepository>();
            IEnumerable<Enterprise> enterprise = repo.Get();
            IEnumerable<EnterpriseModel> models = enterprise.Select(x =>
                new EnterpriseModel
                {
                    Id = x.Id,
                    Name = x.Name,
                    Email = x.Email,
                    Adresse = x.Adresse,
                    Tel = x.Tel,
                    Fax = x.Fax,
                    Tva = x.Tva,
                });
            return models;
        }


        [HttpGet]
        public EnterpriseDetailModel Get(int id)
        {
            EnterpriseRepository repo = ServicesLocator.GetService<EnterpriseRepository>();
            AdvertRepository repoAdvert = ServicesLocator.GetService<AdvertRepository>();
            Enterprise enterprise = repo.GetById(id);
            EnterpriseDetailModel models = new EnterpriseDetailModel
            {
                Id = enterprise.Id,
                Name = enterprise.Name,
                Email = enterprise.Email,
                Adresse = enterprise.Adresse,
                Tel = enterprise.Tel,
                Fax = enterprise.Fax,
                Tva = enterprise.Tva,
                Adverts = repoAdvert.GetByIdEnterprise(id).Select(x =>
                new AdvertModel
                {
                    Id = x.Id,
                    Name = x.Name,
                    Date = x.Date,
                    Description = x.Description,
                    IdEnterprise = x.IdEnterprise
                })
            };
            return models;
        }


        [HttpPost]
        public void Post(EnterpriseModel model)
        {
            EnterpriseRepository repo = ServicesLocator.GetService<EnterpriseRepository>();
            Enterprise models = new Enterprise
            {
                Id = model.Id,
                Name = model.Name,
                Email = model.Name,
                Adresse = model.Adresse,
                Tel = model.Tel,
                Fax = model.Fax,
                Tva = model.Tva,
            };
            repo.Insert(models);
        }



        [HttpPut]
        public void Put(EnterpriseModel x)
        {
            EnterpriseRepository repo = ServicesLocator.GetService<EnterpriseRepository>();
            Enterprise models = new Enterprise
            {
                Id = x.Id,
                Name = x.Name,
                Email = x.Email,
                Adresse = x.Adresse,
                Tel = x.Tel,
                Fax = x.Fax,
                Tva = x.Tva,
            };
            repo.Update(models);
        }


        [HttpDelete]
        public void Delete(int id)
        {
            EnterpriseRepository repo = ServicesLocator.GetService<EnterpriseRepository>();
            repo.Delete(id);
        }
    }
}