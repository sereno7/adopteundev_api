﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AdopteUnDev.Models
{
    public class MessageModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Subject { get; set; }
        public DateTime Date { get; set; }
        public int IdDevelopper { get; set; }
        public int IdAdvert { get; set; }
    }
}