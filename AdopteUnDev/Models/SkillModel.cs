﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AdopteUnDev.Models
{
    public class SkillModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}