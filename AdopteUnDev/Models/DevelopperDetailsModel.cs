﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AdopteUnDev.Models
{
    public class DevelopperDetailsModel
    {
        public int Id { get; set; }
        public string Last_Name { get; set; }
        public string First_Name { get; set; }
        public string Email { get; set; }
        public string Adresse { get; set; }
        public string Tel { get; set; }
        public DateTime Birth_date { get; set; }

        private IEnumerable<AdvertModel> _adverts;
        public IEnumerable<AdvertModel> Adverts
        {
            get { return _adverts; }
            set { _adverts = value; }
        }
    }
}