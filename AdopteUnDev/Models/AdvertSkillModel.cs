﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AdopteUnDev.Models
{
    public class AdvertSkillModel
    {
        public int Id { get; set; }
        public int IdSkill { get; set; }
        public int IdAdvert { get; set; }
    }
}