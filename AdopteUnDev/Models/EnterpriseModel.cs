﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AdopteUnDev.Models
{
    public class EnterpriseModel
    {
		public int Id { get; set; }
		public string Name { get; set; }
		public string Email { get; set; }
		public string Adresse { get; set; }
		public string Tel { get; set; }
		public string Fax { get; set; }
		public string Tva { get; set; }

	}
}