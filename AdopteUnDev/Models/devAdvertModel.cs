﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AdopteUnDev.Models
{
    public class devAdvertModel
    {
        public int Id { get; set; }
        public int IdAdvert { get; set; }
        public int IdDevelopper { get; set; }
        public bool DevAcceptAdvert { get; set; }
        public bool AdvertAcceptDev { get; set; }
    }
}