﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Http.Controllers;

namespace AdopteUnDev.Filters
{

    public class API_AuthorizeAttribute : AuthorizeAttribute
    {
        public override void OnAuthorization(HttpActionContext actionContext)
        {
            actionContext.Request.Headers.TryGetValues("Authorization", out IEnumerable<string> headers);
            string header = headers?.FirstOrDefault();
            if(header == null)
            {
               throw new HttpUnhandledException();
            }
        }
    }
}