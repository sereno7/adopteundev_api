﻿using AdopteUnDev.DAL.Repositories;
using AdopteUnDev.Services;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Security.Cryptography;
using System.Web;
using System.Web.Services.Description;
using ServiceCollection = Microsoft.Extensions.DependencyInjection.ServiceCollection;

namespace AdopteUnDev.DI
{
    public class ServicesLocator
    {
        private static ServiceCollection Services = new ServiceCollection();
        private static ServiceProvider Provider;
        static ServicesLocator()
        {
            string connectionstring = ConfigurationManager.ConnectionStrings["default"].ConnectionString;
            string provider = ConfigurationManager.ConnectionStrings["default"].ProviderName;
            Services.AddTransient(
                (x) => new SkillRepository(connectionstring, provider)
            );
            Services.AddTransient(
                (x) => new EnterpriseRepository(connectionstring, provider)
            );
            Services.AddTransient(
                (x) => new DevelopperRepository(connectionstring, provider)
            );
            Services.AddTransient(
                (x) => new AdvertRepository(connectionstring, provider)
            );
            Services.AddTransient(
                (x) => new DevAdvertRepository(connectionstring, provider)
            );
            Services.AddTransient(
               (x) => new MessageRepository(connectionstring, provider)
           );
            Services.AddTransient(
               (x) => new UserRepository(connectionstring, provider)
           );



            // Service de cryptage register
            // Meilleurs version
            Services.AddSingleton<HashService>();
            Services.AddSingleton<HashAlgorithm, SHA512CryptoServiceProvider>();

            // Service de cryptage
            // alternative moins propre
            //Services.AddSingleton<HashService>(
            //    (x) => new HashService(new SHA512CryptoServiceProvider())
            //    );

            Provider = Services.BuildServiceProvider();
        }
        public static T GetService<T>()
        {
            return Provider.GetService<T>();
        }
    }
}