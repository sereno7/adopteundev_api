﻿CREATE TABLE [dbo].[Enterprise]
(
	[Id] INT NOT NULL PRIMARY KEY IDENTITY,
	[Name] NVARCHAR(50) NOT NULL,
	[Email] NVARCHAR(50) NOT NULL,
	[Adresse] NVARCHAR(50) NOT NULL,
	[Tel] NVARCHAR(50) NOT NULL,
	[Fax] NVARCHAR(50),
	[Tva] VARCHAR(max) NULL,
)
