﻿/*
Modèle de script de post-déploiement							
--------------------------------------------------------------------------------------
 Ce fichier contient des instructions SQL qui seront ajoutées au script de compilation.		
 Utilisez la syntaxe SQLCMD pour inclure un fichier dans le script de post-déploiement.			
 Exemple :      :r .\monfichier.sql								
 Utilisez la syntaxe SQLCMD pour référencer une variable dans le script de post-déploiement.		
 Exemple :      :setvar TableName MyTable							
               SELECT * FROM [$(TableName)]					
--------------------------------------------------------------------------------------
*/
INSERT INTO [Skill] VALUES
('C#'),
('Angular'),
('WPF'),
('ASP')

INSERT INTO [Enterprise] VALUES (
	'STEPUP',
	'stepup@mail.com',
    'Chericou',
	'083634202',
	null,
	'BE7894563563'
)

INSERT INTO [Developper]  VALUES(
	'Giacomelli',
	'Sereno',
	'sereno@mail.com',
	'Chericou',
	'0496226510',
	'1986-08-28'
)